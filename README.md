# README #

### What is this repository for? ###

This repository is a command line interface to the docker-queue-management API.

### Requirements ####

* Python 2.7.6
* Python requests module


## How do I get set up? ##

Adjust the value of DOCKER_HOST_DEFAULT = 'https://s876cnsm:2350' within the file src/utils/Utils.py to the 
host and port where your docker-queue-management application is running. Afterwards you can run
```bash
make
```
to create a folder docker-queue-cli-${VERSION} inside the directory $PWD/build/usr/local/. You can copy this
folder everywhere you want and run the program. Another option is to create a debian package by using
```bash
make deb
```
which creates the .deb package ls8-docker-queue-cli-{VERSION}.deb in the current directory. 
This package can be installed using
```bash
dpkg -i ls8-docker-queue-cli-{VERSION}.deb
```

## How to run ? ##

If you installed the program as debian package, you can just run 
```bash
dockerq [GLOBAL OPTIONS] subcommand [SUBCOMMAND OPTIONS]
```
The options and subcommands are described below.
If you didn't use the debian package, you either have to append the the folder docker-queue-cli-{$VERSION} to your path
or invoke the program dockerQ.py by its absolute path.

By default the program uses tls. If no commandline options are set, it uses $HOME/.docker/ as CA_BUNDLE_PATH. If an 
environment variable named ```DOCKER_CERT_PATH``` is set, it uses the value of this variable as
CA_BUNDLE_PATH. This behaviour can be overwritten by either provide the --ca-bundle-path global option or providing the 
individual paths to the key, cert and ca via the associated options. 

### Global options ###

* -H DOCKER_HOST                       - Address of the queue-management application.
* -d --disable-tls                     - Disable communication vis tls.
* -b | --ca-bundle-path CA_BUNDLE_PATH - Path to a folder containing the certificates {ca,cert,key}.pem
* --cert /path/to/cert.pem             - Path to the certificate.
* --key /path/to/key.pem               - Path to the key.
* --ca /path/to/ca.pem                 - Path to the ca.

### Possible Commands ###

* ps            - Prints the current schedule.
* inspect       - Get information about the job.
* rm            - Delete one or more jobs.
* logs          - List error reports.
* rme           - Removes one or more error reports.

### TODO ###

* Expand Makefile with an option to set DOCKER_HOST_DEFAULT directly. (e.g. sed in src/utils/Utils.py)
* Describe options for the subcommands. 
