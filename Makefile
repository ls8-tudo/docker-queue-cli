BUILD_DIR	=./build
SRC_DIR		=./src
DEB_DIR		=./DEBIAN
DEB_TMPL	=./deb-template/$(DEB_DIR)
VERSION		=$$( cat VERSION )
DBASE_DIR	=/usr/local
INST_DIR	=$(BUILD_DIR)$(DBASE_DIR)
PROG		=docker-queue-cli-$(VERSION)

debfiles        =postinst postrm prerm control
bashsrcfile     =cd $(SRC_DIR); find . -type f -name 'dockerQ' -print
pysrcfiles      =cd $(SRC_DIR); find . -type f -name '*.py' -print

cppyfile: prepbuild
	@ echo "[ enter target cppyfile: copy python files ]" >&2
	set -x; set -v; \
		$(CRD); \
		$(INSTF); \
		_e=0; \
		_srcd="$(SRC_DIR)"; \
		_dstd="$(INST_DIR)/$(PROG)"; \
		{ ( $(pysrcfiles) ); } \
		| while read _i; do \
		echo "_i:$${_i}">&2 ; \
		instfile "$(SRC_DIR)" $(INST_DIR)/$(PROG) "$${_i}" "0755" \
		|| { _e=${?}; break; }; \
		done; \
		[ $${_e} -eq 0 ] || ( echo "ERROR($${_e}) copieing files failed." >&2 ; exit $${_e} )
	@ echo "[ leave target cppyfile: copy python files ]" >&2



prepbuild: 
	@ echo "[ enter target prepbuild: creating directories ]" >&2
	set -x; set -v; \
		$(CRD) ; \
		_e=0; \
		crd "$(BUILD_DIR)/DEBIAN" "0755" && \
		crd "$(INST_DIR)/$(PROG)" "0755" \
		_e=$${?}; \
		[ $${_e} -eq 0 ] || ( echo "ERROR($${_e}) creating directories failed." >&2 ; exit $${_e} )
	@ echo "[ leave target prepbuild: creating directories ]" >&2

debbuild: prepbuild
	@ echo "[enter target debbuild: ]" >&2
	set -x; set -v; \
		_e=0; \
		_v="$(VERSION)"; \
		_dp="$(DBASE_DIR)/$(PROG)"; \
		for _el in $(debfiles); do \
		sed -e "s#@@VERSION@@#$${_v}#g" \
		-e "s#@@INSTALLDIR@@#$${_dp}#g" \
		"$(DEB_TMPL)/$${_el}" > "$(BUILD_DIR)/$(DEB_DIR)/$${_el}" \
		&& if [ "$${_el}" = "control" ]; then \
		chmod 0644 "$(BUILD_DIR)/$(DEB_DIR)/$${_el}"; \
		else \
		chmod 0755 "$(BUILD_DIR)/$(DEB_DIR)/$${_el}"; \
		fi \
		||  { _e=${?}; break; }; \
		done; \
		[ $${_e} -eq 0 ] || ( echo "ERROR($${_e}) creating DEBIAN files failed." >&2 ; exit $${_e} )
	@ echo "[leave target debbuild: ]" >&2

build:	cppyfile debbuild
	@ echo "[enter target build: ]" >&2
	set -x; set -v;  \
		_e=0; \
		_dp="$(DBASE_DIR)/$(PROG)/dockerQ"; \
		_sp="$(SRC_DIR)/dockerQ"; \
		sed  -e "s#@@DOCKERQ@@#$${_dp}.py#g" "$${_sp}" > "$(INST_DIR)/$(PROG)/dockerQ" \
		&& chmod 0775 "$(INST_DIR)/$(PROG)/dockerQ" \
		|| { _e=$${?}; }; \
		[ $${_e} -eq 0 ] || ( echo "ERROR($${_e}) failed to create dockerq shell script." >&2 ; exit $${_e} )
	@ echo "[leave target build: ]" >&2

deb:	clean build
	set -x; set -v; \
		dpkg -b $(BUILD_DIR) ls8-docker-queue-cli-$(VERSION).deb

install:  
	@ dpkg -i ls8-docker-queue-cli-$(VERSION).deb

clean:
	@ echo "[enter target clean]" >&2
	@ rm -r $(BUILD_DIR)
	@ echo "[leave target clean]" >&2

INSTF   = \
		  instfile () { \
		  local _e=0; \
		  local _sbd="$${1}"; \
		  local _dbd="$${2}"; \
		  local _f="$${3}"; \
		  local _m="$${4}"; \
		  local _bn; \
		  local _dn; \
		  _dn=$$( dirname $${_f} ); \
		  _bn=$$( basename $${_f} ); \
		  [ -d "$${_dbd}/$${_dn}" ] \
		  || ( \
		  cd "$${_dbd}" \
		  &&  crd "$${_dn}" "0755" \
		  ) || _e=${?}; \
		  [ $${_e} -eq 0 ] \
		  && cp "$${_sbd}/$${_f}" "$${_dbd}/$${_f}" \
		  && chmod "$${_m}" "$${_dbd}/$${_f}" \
		  || _e=$${?}; \
		  return $${_e}; \
		  }

CRD	= \
	  crd () { \
	  local _e=0; \
	  local _p="$${1}"; \
	  local _m="$${2}"; \
	  local _bn; \
	  local _dn; \
	  if [ -d "$${_p}" ]; then \
	  :; \
	  else \
	  _bn="$$( basename $${_p} )"; \
	  _dn="$$( dirname $${_p} )"; \
	  if [ "$${_bn}" = "." -o "$${_bn}" = "$${_dn}" ]; then \
	  :; \
	  else \
	  crd "$${_dn}" "$${_m}" && \
	  /bin/mkdir -m "$${_m}" "$${_p}" && \
	  /bin/chmod a-s  "$${_p}"; \
	  _e=$${?}; \
	  fi; \
	  fi; \
	  return $${_e}; \
	  }


