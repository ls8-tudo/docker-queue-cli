#!/usr/bin/python
import logging

import requests

logger = logging.getLogger('dockerQ')


class Command(object):
    """
    Base Command-class. Each subcommand-object inherits from this one, to get access to the tls_config and some methods
    to perform Http-requests.
    """

    def __init__(self, tls_config, unknown_args):
        self.tls_config = tls_config
        self.unknown_args = unknown_args
        self.gb_in_bytes = 1073741824

    def execute(self):
        """
        Will be overridden by a specific implementation.
        """
        raise NotImplementedError('Implement me.')

    def api_call(self, api_address, request_method):
        """
        Wrapper for HTTP-Requests. An api_address and the request-method can be specified.
        :param api_address: Address for request.
        :param request_method: Type of Http-Request.
        :return: response-object,
        """
        logger.debug('HTTP-Request to ' + api_address + '.')
        if request_method == 'GET':
            return self.api_call_get(api_address)
        elif request_method == 'DELETE':
            return self.api_call_delete(api_address)
        else:
            print('Http-Request-Method not supported.')
            exit(5)

    def api_call_delete(self, api_address):
        """
        Performs a DELETE-Http-request to the given api_address based on the tls_config of the command.
        :param api_address: Address to send request to
        :return: response-object
        """
        if self.tls_config.use_tls:
            response = requests.delete(api_address, cert=(self.tls_config.cert, self.tls_config.key),
                                       verify=False)
        else:
            response = requests.delete(api_address)
        return response

    def api_call_get(self, api_address):
        """
        Performs a GET-Http-request to the given api_address based on the tls_config of the command.
        :param api_address: Address to send request to
        :return: response-object
        """
        if self.tls_config.use_tls:
            response = requests.get(api_address, cert=(self.tls_config.cert, self.tls_config.key),
                                    verify=False)
        else:
            response = requests.get(api_address)
        return response
