#!/usr/bin/python

from subcommands.InspectJobCommand import InspectJobCommand
from subcommands.RmErrorCommand import RmErrorCommand
from subcommands.RmJobCommand import RmJobCommand
from subcommands.ShowErrorCommand import ShowErrorCommand
from subcommands.ShowJobsCommand import ShowJobsCommand

"""
This module is a "proxy" for the incoming sub-commands. Here commands can be registered for usage in the commandline.
Each command features its own argument parser, so that dockerQ.py does not need to handle the arguments for each
command.
"""


def inspect(*args):
    """
    Inspect a job.
    """
    InspectJobCommand(*args).execute()


def rme(*args):
    """
    Remove an error-report.
    """
    RmErrorCommand(*args).execute()


def rm(*args):
    """
    Removes a job from the queue.
    """
    RmJobCommand(*args).execute()


def logs(*args):
    """
    Return the error-reports for one or more jobs.
    """
    ShowErrorCommand(*args).execute()


def ps(*args):
    """
    List the current jobs.
    """
    ShowJobsCommand(*args).execute()
