#!/usr/bin/python

import argparse
import datetime
import json
import sys

import utils.Utils as Utils
from commands.Command import Command
from utils.exceptions import AuthenticationException


class ShowJobsCommand(Command):
    """
    Lists all jobs in the queue. If the argument --user is used, only the jobs for the current user are displayed. The
    -v option
    """

    def execute(self):
        parser = argparse.ArgumentParser(
            description='''Lists the jobs in the queue. If the argument --user is used, it only shows the jobs for
            the specified user. The argument -v can be used to show a complete json output of the jobs.'''
        )

        parser.add_argument('-u', '--user', help='Only show jobs for the selected user.')
        parser.add_argument('-v', '--verbose', action='store_true', help='Full json output.')

        args, unknown = parser.parse_known_args(self.unknown_args)

        api_address = self.tls_config.host + '/schedule/json'

        response = self.api_call(api_address, request_method='GET')

        if response.status_code == 403:
            raise AuthenticationException

        if response.status_code != 200:
            print (response.text)
            sys.exit(response.status_code)

        my_list = []
        col_list = ['Position', 'Id', 'Owner', 'Container-name', 'Image',
                    'Submitted', 'CPU', 'Memory (GB)']
        i = 0
        json_data = json.loads(response.text)
        if args.verbose and not args.user:
            print json.dumps(response.json(), indent=4)
            sys.exit(0)
        if args.verbose:
            for json_obj in json_data:
                if args.user and json_obj['jobOwner'] == args.user:
                    my_list.append(json_obj)
            print json.dumps(my_list, indent=4)
        else:
            for json_obj in json_data:
                i += 1
                # If i don't match the user, i want to update i but not render an object.
                if args.user and json_obj['jobOwner'] != args.user:
                    continue
                json_obj = {
                    'Position': i,
                    'Id': json_obj['id'],
                    'Owner': json_obj['jobOwner'],
                    'Container-name': str(json_obj['containerName']),
                    'Image': json_obj['Image'],
                    'Submitted': str(datetime.datetime.fromtimestamp(json_obj['created'] / 1000)),
                    'CPU': str(json_obj['HostConfig']['CpuShares']),
                    'Memory (GB)': str((json_obj['HostConfig']['Memory'] / float(self.gb_in_bytes))),
                    'usage': json_obj['resourceUsage']
                }
                my_list.append(json_obj)
            Utils.print_table(my_list, col_list)
        sys.exit(0)
