#!/usr/bin/python

import argparse
import logging
import sys

from commands.Command import Command

logger = logging.getLogger('dockerQ')


class RmErrorCommand(Command):
    """
    This class is used to delete error-reports from the queue-management-application. It either gets one or multiple
    id`s to delete jobs for or a user can specify to delete all error-reports for himself / all users including him.

    """

    def execute(self):
        """
        Parses command line options and initiates api calls.
        :return:
        """
        parser = argparse.ArgumentParser(
            description='Deletes an error-report by default on the given id. If --user or --all is specified, '
        )
        # Id is the default argument, but if --user or --all is specified, it can be omitted.
        parser.add_argument('id', help='Delete a error-report based on it\'s id.', nargs='*', default=None)

        # Add the options to delete all error-reports for one user or all user`s.
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-a', '--all', action='store_true', help='Delete all error-reports for all users.')
        group.add_argument('-u', '--user', help='Delete all error-reports for one user.')

        args, unknown = parser.parse_known_args(self.unknown_args)

        api_address = self.tls_config.host + '/errors'

        # Id and all or user are given
        if args.id and (args.user or args.all):
            print('Choose to delete based on id or user/all.')
            sys.exit(-1)
        if args.all:
            # Verify user to delete all requests
            var = raw_input('You are going to remove all the error-reports. Are you sure? Enter [y|n].\n')
            if var == 'y':
                api_address += '/all/delete'
                response = self.api_call(api_address, request_method='DELETE')
                print(response.text)
            else:
                sys.exit(0)
        elif args.user:
            # Verify user to delete all his requests
            var = raw_input('You are going to remove all the error-reports for the user ' + str(args.user) +
                            ' in the queue. Are you sure? Enter [y|n].\n')
            if var == 'y':
                api_address += '/all/' + args.user + '/delete'
                response = self.api_call(api_address, request_method='DELETE')
                print(response.text)
            else:
                sys.exit(0)
        elif not args.id:
            print('If no id is given, you either have to specify --user USER or --all.')
            sys.exit(-1)

        # Now i can delete the ids

        for id in args.id:
            api_address = self.tls_config.host + '/errors/' + id + '/delete'
            response = self.api_call(api_address, request_method='DELETE')
            if response.status_code == 403:
                print('You are not allowed to delete the error-report with id ' + str(id) + '.')
                continue
            print(response.text)

        sys.exit(0)
