#!/usr/bin/python

import argparse
import json
import logging

from commands.Command import Command

logger = logging.getLogger('dockerQ')


class InspectJobCommand(Command):
    """
    Calls the inspect endpoint for one or multiple ids.
    """

    def execute(self):
        parser = argparse.ArgumentParser(
            description='Inspect a job for one or more id\'s.'
        )

        # I am expecting one or more arguments of id`s.
        parser.add_argument('jobId', help='Identifier for the jobs to inspect.', nargs='+')
        # Optional print pure json.
        parser.add_argument('-s', '--suppress', action='store_true',
                            help='Suppress error messages and only print json-array.')

        args, unknown = parser.parse_known_args(self.unknown_args)

        error_list = []
        json_list = []

        for job_id in args.jobId:
            api_address = self.tls_config.host + '/jobs/' + job_id + '/inspect'
            logger.debug('HTTP-Request to ' + api_address)
            response = self.api_call(api_address, request_method='GET')

            if response.status_code == 403:
                error_list.append('You are not allowed to inspect job ' + str(job_id) + '.')
            if response.status_code != 200:
                error_list.append(response.text)
            else:
                json_list.append(response.json())

        if len(json_list) != 0:
            print(json.dumps(json_list, indent=4))

        if not args.suppress:
            if len(error_list) > 0:
                print 'Some failure(s) happened: '
                for error in error_list:
                    print error

        exit(0)
