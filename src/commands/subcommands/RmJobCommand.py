#!/usr/bin/python

import argparse
import sys

from commands.Command import Command


class RmJobCommand(Command):
    """
    Deletes jobs from the queue. It can delete all jobs for all users, all jobs for one user or one to multiple jobs,
    based on their id.
    """

    def execute(self):
        parser = argparse.ArgumentParser(
            description='Deletes a job from the queue. One argument has to be passed to specify the removal.'
        )

        # Id is the default argument, but if --user or --all is specified, it can be omitted.
        parser.add_argument('id', help='Delete a error-report based on it\'s id.', nargs='*', default=None)

        group = parser.add_mutually_exclusive_group()

        group.add_argument('-a', '--all', action='store_true', help='Deletes all jobs.')
        group.add_argument('-u', '--user', help='Deletes all jobs for an user.')

        args, unknown = parser.parse_known_args(self.unknown_args)

        api_address = self.tls_config.host + '/jobs'

        # Id and all or user are given
        if args.id and (args.user or args.all):
            print('Choose to delete based on id or --user or --all.')
            sys.exit(-1)
        if args.all:
            # Verify user to delete all requests
            var = raw_input('You are going to remove all the jobs in the queue. Are you sure? Enter [y|n].\n')
            if var == 'y':
                api_address += '/all/delete'
                response = self.api_call(api_address, request_method='DELETE')
                print(response.text)
            else:
                sys.exit(0)
        elif args.user:
            # Verify user to delete all his requests
            var = raw_input('You are going to remove all the jobs for the user ' + str(args.user) +
                            ' in the queue. Are you sure? Enter [y|n].\n')
            if var == 'y':
                api_address += '/all/' + args.user + '/delete'
                response = self.api_call(api_address, request_method='DELETE')
                print(response.text)
            else:
                sys.exit(0)
        elif not args.id:
            print('If no id is given, you either have to specify --user USER or --all.')
            sys.exit(-1)

        for job_id in args.id:
            api_address = self.tls_config.host + '/jobs/' + job_id + '/delete'
            response = self.api_call(api_address, request_method='DELETE')
            if response.status_code == 403:
                print('You are not allowed to delete the job with id ' + str(job_id) + '.')
                continue
            print(response.text)

        sys.exit(0)
