#!/usr/bin/python

import argparse
import json
import sys

import utils.Utils as Utils
from commands.Command import Command
from utils.exceptions import AuthenticationException


class ShowErrorCommand(Command):
    """
    Requests the error-reports, depending on the set parameter and prints them out.
    """

    def execute(self):
        parser = argparse.ArgumentParser(
            description='Lists error reports for aborted jobs. With no argument specified,'
                        ' this command returns the errors for the current user.'
        )
        # Determine if reports for a specific or all user should be send.
        group = parser.add_mutually_exclusive_group()
        group.add_argument('-a', '--all', action='store_true', help='Show all errors for all users.')
        group.add_argument('-u', '--user', help='Show all errors for one specified user.')

        parser.add_argument('-v', '--verbose', help='Prints full json output.', action='store_true')

        args, unknown = parser.parse_known_args(self.unknown_args)

        api_address = self.tls_config.host + '/errors'
        if args.user:
            api_address += '/' + args.user + '/json'
        elif args.all:
            api_address += '/json'
        else:
            api_address += '/' + Utils.get_username() + '/json'

        response = self.api_call(api_address, request_method='GET')
        if response.status_code == 403:
            raise AuthenticationException

        if response.status_code != 200:
            print ('No error reports found for your request.')
            sys.exit(0)

        # no error occurred
        if args.verbose:
            # print complete json
            print json.dumps(response.json(), indent=4)
        else:
            json_data = json.loads(response.text)
            col_list = ['Id', 'Owner', 'Container-name', 'Image', 'Error-message', 'Command']
            error_list = list()
            for json_error in json_data:
                json_obj = {
                    'Id': str(json_error['id']),
                    'Owner': json_error['owner'],
                    'Container-name': str(json_error['job']['containerName']),
                    'Image': json_error['job']['Image'],
                    'Command': str(' '.join(json_error['job']['Cmd'])),
                    'Error-message': json_error['errorMessage']
                }
                error_list.append(json_obj)
            Utils.print_table(error_list, col_list)
