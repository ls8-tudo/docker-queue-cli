#!/usr/bin/python

import argparse
import logging
import warnings

import requests

import utils.Utils as Utils
from commands import commands
from utils.exceptions import AuthenticationException

logger = logging.getLogger('dockerQ')


def usage_msg():
    """
    Usage message for available subcommands
    """
    return """dockerQ [-H DOCKER_HOST] [--disable-tls] [ -b CA_BUNDLE_PATH | --cert CERT --key KEY --ca CA] <subcommand> <args>

Possible subcommands are:

            inspect    - Inspect a job in the queue.
            logs       - Show aborted jobs.
            ps         - Show jobs in the queue.
            rm         - Remove a job from the queue.
            rme        - Remove error message(s) for aborted jobs.

For more information about a subcommand, you can call:

            dockerQ.py <subcommand> -h

optional arguments:
  -h, --help            show this help message and exit
  -H DOCKER_HOST, --docker-host DOCKER_HOST
                        Address of queue-management-application.
  -d, --disable-tls     Disable communication via TLS.
  -b CA_BUNDLE_PATH, --ca-bundle-path CA_BUNDLE_PATH
                        Path to certificate-folder. Assumes ca.pem, cert.pem
                        and key.pem exist within this folder.
  --cert CERT           Path to cert.
  --key KEY             Path to key.
  --ca CA               Path to ca.
"""


def main():
    # Create main parser
    parser = argparse.ArgumentParser(
        description="docker-queue-cli",
        add_help=False,
        usage=usage_msg()
    )

    parser.add_argument('-H', '--docker-host', help='Address of queue-management-application.', default=None)

    parser.add_argument('-d', '--disable-tls', help='Disable communication via TLS.', action='store_true',
                        default=False)
    parser.add_argument('-b', '--ca-bundle-path',
                        help='Path to certificate-folder. Assumes {ca,cert,key}.pem exist within this folder.')
    parser.add_argument('--cert',
                        help='Path to cert.')
    parser.add_argument('--key',
                        help='Path to key.')
    parser.add_argument('--ca',
                        help='Path to ca.')

    parser.add_argument('--debug', help='Print debug messages.', action='store_true')

    parser.add_argument('subcommand', help='Subcommand to run')

    args, unknown = parser.parse_known_args()

    # because Python < 2.7.9, urllib3 throws some warnings. To show them comment the following line.

    warnings.filterwarnings("ignore")

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
        logger.setLevel(logging.DEBUG)
    logger.debug('Debug logging level activated.')

    tls_config = Utils.handle_tls_arguments(args)

    if not hasattr(commands, args.subcommand):
        print('Unrecognized subcommand.\n')
        print(usage_msg())
        exit(1)
    # let command class handle the command
    try:
        getattr(commands, args.subcommand)(tls_config, unknown)
    except AuthenticationException as e:
        print 'You are not allowed to perform the requested action.'
    except requests.exceptions.SSLError as e:
        print e
        print "Error: something with your certificate was wrong."
    except requests.exceptions.ConnectionError as e:
        print e
        print "Error : Could not connect to queue-host."


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt as e:
        print 'Exited with ctrl+c.'
        exit(0)
