import logging
import os
import pwd
import sys

DOCKER_HOST_DEFAULT = 'https://s876cnsm:2350'
DOCKER_HOST_API = '/v1.30'

logger = logging.getLogger('dockerQ')


class TLSConfig(object):
    """
    This object is used to create a request call based on the tls-settings.
    """

    def __init__(self, host=None, cert=None, key=None, ca=None, use_tls=False):
        self.host = host
        self.use_tls = use_tls
        self.cert = cert
        self.key = key
        self.ca = ca


# Logic to parse files for tls settings
def handle_tls_arguments(args):
    tls_config = TLSConfig()

    user_name = get_username()
    user_home_dir = '/home/' + user_name

    host = DOCKER_HOST_DEFAULT + DOCKER_HOST_API

    cert_path_set = os.environ.get('DOCKER_CERT_PATH') is not None

    if args.docker_host is not None:
        # Only custom check, if -H flag is set.
        # If not using tls, append http to address
        if args.disable_tls and not args.docker_host.startswith('http://'):
            host = 'http://' + args.docker_host + DOCKER_HOST_API
        elif not args.disable_tls and not args.docker_host.startswith('https://'):
            host = 'https://' + args.docker_host + DOCKER_HOST_API

    # If TLS disabled, dont care about the rest.
    if args.disable_tls:
        logger.debug('Disabled TLS')
        tls_config.host = host
        tls_config.use_tls = False
        return tls_config

    # Nothing is set => Load default {ca,cert,key}.pem from ~/.docker folder of user
    if not args.ca_bundle_path and not (args.cert or args.key or args.ca):
        # if DOCKER_CERT_PATH is set, i load the keys from here
        tls_config.host = host
        tls_config.use_tls = not args.disable_tls
        if cert_path_set:
            cert_path_value = os.environ.get('DOCKER_CERT_PATH')
            if not cert_path_value.endswith('/'):
                cert_path_value += '/'
            logger.debug('Loading certs from DOCKER_CERT_PATH ' + cert_path_value + '.')
            tls_config.ca = cert_path_value + 'ca.pem'
            tls_config.cert = cert_path_value + 'cert.pem'
            tls_config.key = cert_path_value + 'key.pem'
        # if the DOCKER_CERT_PATH is not  set, i load the keys from ~/.docker
        else:
            logger.debug('Loading certs from ~/.docker/')
            tls_config.ca = user_home_dir + '/.docker/ca.pem'
            tls_config.cert = user_home_dir + '/.docker/cert.pem'
            tls_config.key = user_home_dir + '/.docker/key.pem'
    elif args.ca_bundle_path and not (args.cert or args.key or args.ca):
        path = args.ca_bundle_path
        if not path.endswith('/'):
            path += '/'
            logger.debug('Loading certificates from CA-bundle-path.')
        tls_config.host = host
        tls_config.use_tls = not args.disable_tls
        tls_config.ca = path + 'ca.pem'
        tls_config.cert = path + 'cert.pem'
        tls_config.key = path + 'key.pem'
    elif args.ca_bundle_path and (args.cert or args.key or args.ca):
        print "Either choose --ca-bundle or set the paths to each file."
        sys.exit(-1)
        # Here bundle and one of the others is set
    elif not args.ca_bundle_path and (args.cert or args.key or args.ca):
        print 'To specify a custom location, you have to provide a ca, a key and a cert.'
        sys.exit(-1)
    else:
        logger.debug('Unknown case.')
        sys.exit(-2)
    logger.debug('\n'.join("%s: %s" % item for item in vars(tls_config).items()))
    return tls_config


def print_table(my_dict, col_list=None):
    """
    Pretty print a list of dictionaries (myDict) as a dynamically sized table.
    If column names (colList) aren't specified, they will show in random order.
    Author: Thierry Husson - Use it as you want but don't blame me.
    """
    if not col_list:
        col_list = list(my_dict[0].keys() if my_dict else [])
    my_list = [col_list]  # 1st row = header
    for item in my_dict:
        my_list.append([str(item[col] or '') for col in col_list])
    col_size = [max(map(len, col)) for col in zip(*my_list)]
    format_str = ' | '.join(["{{:<{}}}".format(i) for i in col_size])
    # Separating line
    my_list.insert(1, ['-' * i for i in col_size])

    for item in my_list:
        print (format_str.format(*item))


def get_username():
    return pwd.getpwuid(os.getuid()).pw_name
