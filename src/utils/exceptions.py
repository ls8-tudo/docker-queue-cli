class AuthenticationException(Exception):
    """
    Raise this exception, if the user is not authorized to call a specific endpoint.
    """
    pass
